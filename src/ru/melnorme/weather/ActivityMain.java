package ru.melnorme.weather;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.concurrent.ExecutionException;


public class ActivityMain extends Activity implements OnClickListener {
    public static final String LOG_TAG = "myLogs";
    public static final String CITY_LIST = "cityList";
    public static final String CITY_ID = "cityId";
    private DownloadXmlTask xmlTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        xmlTask = new DownloadXmlTask();
        xmlTask.execute(CITY_LIST);
        //xmlTask.execute(CITY_ID, "27002");
/*
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                String xml = null;
                try {
                    xml = xmlTask.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                //Parser.parseXml(xml);
                //Log.d(LOG_TAG, xml);
            }
        });
        t1.start();*/
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnGo:
                Intent actResult = new Intent(this, ActivityResult.class);
                startActivity(actResult);
                break;
            case R.id.btnCityList:
                xmlTask = new DownloadXmlTask();
                xmlTask.execute(CITY_LIST);
                break;
            case R.id.btnCityId:
                xmlTask = new DownloadXmlTask();
                xmlTask.execute(CITY_ID, "27002");
                break;
        }
    }
}
