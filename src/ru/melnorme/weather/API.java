package ru.melnorme.weather;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;

public interface API {
    @GET("/cities.xml")
    void getCityList(Callback<Response> callback);

    @GET("/{city_id}.xml")
    void getCityWeather(@Path("city_id") String city_id, Callback<Response> callback);
}
