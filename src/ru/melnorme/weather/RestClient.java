package ru.melnorme.weather;

import android.util.Log;
import com.squareup.okhttp.OkHttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.MimeUtil;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

public class RestClient {
    private static API CLIENT_CITY_LIST;
    private static API CLIENT_CITY;
    private static String CITY_LIST_URL = "https://pogoda.yandex.ru/static";
    private static String CITY_URL = "http://export.yandex.ru/weather-ng/forecasts";

    static {
        setupRestClient();
    }

    public static API getCityList() {
        return CLIENT_CITY_LIST;
    }

    public static API getCity() {
        return CLIENT_CITY;
    }

    private static void setupRestClient() {
        CLIENT_CITY = createApi(CITY_URL);
        CLIENT_CITY_LIST = createApi(CITY_LIST_URL);
    }

    private static API createApi(String url) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        return restAdapter.create(API.class);
    }

    public static String stringFromResponse(Response response){
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(response.getBody().in(), "UTF-8"));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        String result = sb.toString();
        return result;
    }

    public static String stringFromResponse2(Response response) {
        long bodySize1 = 0L;
        TypedInput body = response.getBody();
        if(body != null) {
            byte[] bodyBytes = ((TypedByteArray)body).getBytes();
            bodySize1 = (long)bodyBytes.length;
            String bodyMime = body.mimeType();
            String bodyCharset = MimeUtil.parseCharset(bodyMime, "UTF-8");
            try {
                return new String(bodyBytes, bodyCharset);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
