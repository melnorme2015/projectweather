package ru.melnorme.weather;

import android.text.TextUtils;
import android.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.ExecutionException;

public class Parser {

    public static XmlPullParser prepareXpp(String s) throws XmlPullParserException {
        // �������� �������
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        // �������� ��������� namespace (�� ��������� ���������)
        factory.setNamespaceAware(true);
        // ������� ������
        XmlPullParser xpp = factory.newPullParser();
        // ���� ������� �� ���� Reader
        xpp.setInput(new StringReader(s));
        return xpp;
    }

    public static void parseXml(String s) {
        String tmp = "";
        try {
            //XmlPullParser xpp = getResources().getXml(R.xml.city);
            XmlPullParser xpp = prepareXpp(s);
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    // ������ ���������
                    case XmlPullParser.START_DOCUMENT:
                        Log.d(ActivityMain.LOG_TAG, "START_DOCUMENT");
                        break;
                    // ������ ����
                    case XmlPullParser.START_TAG:
                        Log.d(ActivityMain.LOG_TAG, "START_TAG: name = " + xpp.getName()
                                + ", depth = " + xpp.getDepth() + ", attrCount = "
                                + xpp.getAttributeCount());
                        tmp = "";
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            tmp = tmp + xpp.getAttributeName(i) + " = "
                                    + xpp.getAttributeValue(i) + ", ";
                        }
                        if (!TextUtils.isEmpty(tmp))
                            Log.d(ActivityMain.LOG_TAG, "Attributes: " + tmp);
                        break;
                    // ����� ����
                    case XmlPullParser.END_TAG:
                        Log.d(ActivityMain.LOG_TAG, "END_TAG: name = " + xpp.getName());
                        break;
                    // ���������� ����
                    case XmlPullParser.TEXT:
                        Log.d(ActivityMain.LOG_TAG, "text = " + xpp.getText());
                        break;
                    default:
                        break;
                }
                // ��������� �������
                xpp.next();
            }
            Log.d(ActivityMain.LOG_TAG, "END_DOCUMENT");
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
