package ru.melnorme.weather;

import android.os.AsyncTask;
import android.util.Log;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DownloadXmlTask extends AsyncTask<String, String, String> {
    private String s = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        if(strings[0].equals(ActivityMain.CITY_LIST)){
            RestClient.getCityList().getCityList(new Callback<Response>() {
                @Override
                public void success(Response r0, Response response) {
                    s = RestClient.stringFromResponse(response);
                    //Parser.parseXml(s);
                    Log.d(ActivityMain.LOG_TAG, s);
                }

                @Override
                public void failure(RetrofitError error) {
                    s = "Unknown error";
                }
            });
        }

        if(strings[0].equals(ActivityMain.CITY_ID)) {
            RestClient.getCity().getCityWeather(strings[1], new Callback<Response>() {
                @Override
                public void success(Response r0, Response response) {
                    s = RestClient.stringFromResponse(response);
                    //Parser.parseXml(s);
                    Log.d(ActivityMain.LOG_TAG, s);
                }

                @Override
                public void failure(RetrofitError error) {
                    s = "Unknown error";
                }
            });
        }
        return s;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
}
